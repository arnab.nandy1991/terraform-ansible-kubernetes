variable "key_name" {
  description = "Desired name of AWS key pair"
  default = "KeyPair_nandykarnab_29032020_ap-south-1"
}

variable "aws_avail_zone" {
  description = "AWS Availability Zone"
  default = "ap-south-1a"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "ap-south-1"
}


variable "aws_amis" {
  default = {
    us-east-1 = "ami-0470e33cd681b2476"
    ap-south-1 = "ami-0465196b"
  }
}

variable "awsprops" {
    type = "map"
    default = {
        region = "ap-south-1"
        vpc = "vpc-5234832d"
        ami = "ami-0c1bea58988a989155"
        itype = "t2.medium"
        subnet = "subnet-81896c8e"
        publicip = true
        keyname = "myseckey"
        secgroupname = "K8s-SG"
  }
}
